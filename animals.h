///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   02_02_2021
///////////////////////////////////////////////////////////////////////////////


#pragma once
#include <stdbool.h>
/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

/// Gender is appropriate for all animals in this database
// enum Gender // @todo fill this out from here...
enum Gender {MALE, FEMALE};

char* genderString (enum Gender gender);
/// Return a string for the name of the color
/// @todo this is for you to implement
enum Color {BLACK, WHITE, RED, BLUE, GREEN, PINK};

char* colorName (enum Color color);


enum CatBreeds {MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

char* catBreed (enum CatBreeds breed);

char* catFixed (bool fixed);
